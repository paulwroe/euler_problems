from problems_1_to_10 import multiples_of_3_or_5 as one
from problems_1_to_10 import even_fibonacci_numbers as two
from problems_1_to_10 import largest_prime_factor as three
from problems_1_to_10 import largest_palindrome_product as four
from problems_1_to_10 import smallest_multiple as five
from problems_1_to_10 import sum_square_difference as six
from problems_1_to_10 import ten_thousand_and_first_prime as seven


def test_multiples_of_3_or_5():
    assert one.multiples_of_3_or_5(10) == 23


def test_even_fibonacci_numbers():
    sum_of_first_ten_even_numbers = 44
    assert two.even_fibonacci_numbers(10) == sum_of_first_ten_even_numbers


def test_largest_prime_factor():
    assert three.largest_prime_factor(13195) == 29


def test_largest_palindrome_product():
    assert four.largest_palindrome_product(2) == 9009


def test_smallest_multiple():
    assert five.smallest_multiple(10) == 2520


def test_sum_square_difference():
    assert six.sum_square_difference(10) == 2640


def test_ten_thousand_and_first_prime():
    assert seven.ten_thousand_and_first_prime(6) == 13
