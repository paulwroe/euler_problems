#! /usr/bin/env python

'''
2520 is the smallest number that can be divided by each of the numbers
from 1 to 10 without any remainder.
What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
'''

'''
solving by determining the greatest frequency of a prime for any given number in the sequence,
then multiplying only the greatest number of occurances of that prime.
ie. if one number has 2*2*3 for primes and another number has 2*2*2*5 for primes, 3 twos will
be included in the final calculation
'''


prime_dict = {}


def smallest_multiple(upper_range):
    for number in range(upper_range, 0, -1):
        prime_factor_dict = find_prime_factors(number)
        update_prime_dict(prime_factor_dict)
    final_answer = multiply_out_dict(prime_dict)
    return final_answer


def multiply_out_dict(prime_dict):
    product_of_operation = 1
    for prime, number_of_occurances in prime_dict.items():
        product_of_operation = pow(prime, number_of_occurances) * product_of_operation
    return product_of_operation


def update_prime_dict(prime_factor_dict):
    for prime in prime_factor_dict.keys():
        if prime in prime_dict:
            if prime_dict[prime] < prime_factor_dict[prime]:
                prime_dict[prime] = prime_factor_dict[prime]
        else:
            prime_dict[prime] = prime_factor_dict[prime]


def find_prime_factors(input_number):
    working_number = input_number
    prime_dict = {}
    for number in range(2, input_number + 1):
        if working_number % number == 0:
            prime_dict[number] = 0
        while True:
            if working_number % number == 0:
                working_number = working_number / number
                prime_dict[number] = prime_dict[number] + 1
            else:
                break
    return prime_dict


if __name__ == '__main__':
    final_answer = smallest_multiple(20)
    print(f'The final answer is {final_answer}.')
