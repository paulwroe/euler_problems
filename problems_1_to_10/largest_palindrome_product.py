#! /usr/bin/env python

'''
A palindromic number reads the same both ways.
The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 times 99.

Find the largest palindrome made from the product of two 3-digit numbers.
'''


def largest_palindrome_product(number_of_digits):
    range_number = 0
    for number in range(number_of_digits):
        range_number += 9 * 10**number
    palindrome = 0
    for outer_number in range(range_number, 0, -1):
        for inner_number in range(range_number, 0, -1):
            product = outer_number * inner_number
            product_string = str(product)
            reverse_product = product_string[::-1]
            if product_string == reverse_product and palindrome < int(product_string):
                palindrome = int(product_string)
    return palindrome


if __name__ == '__main__':
    final_answer = largest_palindrome_product(3)
    print(f'The final answer is {final_answer}.')
