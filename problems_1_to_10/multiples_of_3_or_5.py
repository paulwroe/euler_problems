#! /usr/bin/env python

'''
Multiples of 3 or 5
https://projecteuler.net/problem=1

If we list all the natural numbers below 10 that are
multiples of 3 or 5, we get 3,5,6 and 9.
The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
'''


def multiples_of_3_or_5(range_number):
    return_value = 0
    for number in range(range_number):
        if (number % 3 == 0) or (number % 5 == 0):
            return_value += number
    return return_value


if __name__ == '__main__':
    final_answer = multiples_of_3_or_5(1000)
    print(f'The final answer is {final_answer}.')
