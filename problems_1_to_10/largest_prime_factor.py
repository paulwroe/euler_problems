#! /usr/bin/env python

'''
The prime factors of 13195 are 5,7,13, and 29.
What is the largest prime factor of the number 600851475143?
'''


def largest_prime_factor(input_number):

    largest_prime_found = 1
    test_number = input_number
    for range_number in range(input_number):
        prime_candidate = range_number + 1
        while True:
            if test_number % prime_candidate == 0 and prime_candidate != 1:
                test_number = test_number / prime_candidate
                if largest_prime_found < prime_candidate:
                    largest_prime_found = prime_candidate
            else:
                break
        if prime_candidate > (input_number/2):
            break
    return largest_prime_found


if __name__ == '__main__':
    final_answer = largest_prime_factor(600851475143)
    #  this takes a real long time to run
    print(f'The final answer is {final_answer}.')
