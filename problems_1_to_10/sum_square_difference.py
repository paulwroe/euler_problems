#! /usr/bin/env python

'''
The sum of the squares of the first ten natural numbers is
1^2 + 2^2 + ... + 10^2 = 385

The square of the sum of the first ten natural numbers is
(1 + 2 + ... + 10)^2 = 55^2 = 3025

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is
3025 - 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
'''


def sum_square_difference(input_number):
    sum_of_the_squares = 0
    sum_of_the_range = 0
    for number in range(1, input_number + 1):
        sum_of_the_squares += pow(number, 2)
        sum_of_the_range += number
    square_of_the_sum_of_the_range = pow(sum_of_the_range, 2)
    difference = square_of_the_sum_of_the_range - sum_of_the_squares
    return difference


if __name__ == '__main__':
    final_answer = sum_square_difference(10)
    print(f'The final answer is {final_answer}.')
